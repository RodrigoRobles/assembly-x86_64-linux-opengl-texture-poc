;-------------------------------------------------------------------------------------
;	texture.asm
;	Assembly x86_64 Linux OpenGL Texture POC
;	To assemble:
;		nasm -felf64 -Wall texture.asm -o texture.o
;	To link:
;		gcc -no-pie -march=native -m64 -O3 -Wall -Wextra texture.o -lGL -lglut -o texture
;	To run:
;		./texture
;-------------------------------------------------------------------------------------

default rel
global main

extern glutInit
extern glutInitDisplayMode
extern glutInitWindowSize
extern glutInitWindowPosition
extern glutCreateWindow
extern glutDisplayFunc
extern glutMainLoop
extern glClearColor
extern glClear
extern glColor3f
extern glOrtho
extern glBegin
extern glVertex3f
extern glEnd
extern glFlush
extern glGenTextures
extern glBindTexture
extern glTexImage2D
extern glTexParameteri
extern glEnable
extern glShadeModel
extern glClearDepth
extern glDepthFunc
extern glHint
extern glTexCoord2f
extern glGetError

;----------------------------------------------------
section .data

    dq_minusone dq -1.0
    dq_one dq 1.0
    winhandle dq 0
    dd_pointfour dd 0.4
    dd_one dd 1.0
    dq_three dd 3
    dd_minuspointseven dd 0.0
    dd_rectsize dd 256.0
    dd_minusone dd -2.0
    dd_zero dd 0.0
    wintitle db "Game Test",0
    dq_screenwidth dq 512.0   ; 1
    dq_screenheight dq 512.0 ;-1
    dq_zero dq 0.0
    dq_bottom dq 0.0
    dq_texturenum dq 0
    GL_TEXTURE_2D dq 3553
    GL_RGB dq 0x1907
    GL_UNSIGNED_BYTE dq 0x1401
    GL_TEXTURE_MAG_FILTER dq 0x2800        
    GL_TEXTURE_MIN_FILTER dq 0x2801
    GL_LINEAR dq 0x2601 
    GL_SMOOTH dq 0x1D01               
    GL_DEPTH_TEST dq 0x0B71
    GL_EQUAL dq 0x0202
    GL_PERSPECTIVE_CORRECTION_HINT dq 0x0C50;	
    GL_NICEST dq 0x1102
    GL_CLEAR_BUFFER_BIT dq 16384
    GL_TRIANGLES dq 4
    GL_DEPTH_BUFFER_BIT dq 0x00000100
    GL_RGB8 dq 32849
    dq_textureside dq 64       

;----------------------------------------------------
section .bss
	texture resb 12288

;----------------------------------------------------
section .text

draw:

	push rbp
	push rbx
	sub rsp, 8

	movd xmm0, dword [dd_pointfour]
	movd xmm1, dword [dd_pointfour]
	movd xmm2, dword [dd_pointfour]
	movd xmm3, dword [dd_pointfour]
	call glClearColor wrt ..plt

	mov rax, [GL_CLEAR_BUFFER_BIT]
	or rax, [GL_DEPTH_BUFFER_BIT]
	mov rdi, rax
	call glClear wrt ..plt

	movq xmm0, qword [dq_minusone]
	movq xmm1, qword [dq_screenwidth]
	movq xmm2, qword [dq_screenheight]
	movq xmm3, qword [dq_bottom]
	movq xmm4, qword [dq_minusone]
	movq xmm5, qword [dq_one]
	call glOrtho wrt ..plt

	mov rdi, [GL_TRIANGLES]
	call glBegin wrt ..plt
	vzeroall

	movd xmm0, dword [dd_zero]
	movd xmm1, dword [dd_one]
	call glTexCoord2f wrt ..plt

	; 0, 256  
	movd xmm0, dword [dd_zero]
	movd xmm1, dword [dd_rectsize]
	call glVertex3f wrt ..plt

	movd xmm0, dword [dd_one]
	movd xmm1, dword [dd_one]
	call glTexCoord2f wrt ..plt

	; 256, 256
	movd xmm0, dword [dd_rectsize]
	movd xmm1, dword [dd_rectsize]
	call glVertex3f wrt ..plt

	pxor xmm0, xmm0
	pxor xmm1, xmm1 
	call glTexCoord2f wrt ..plt

	; 0, 0
	pxor xmm0, xmm0
	pxor xmm1, xmm1 
	call glVertex3f wrt ..plt
		
	pxor xmm0, xmm0
	pxor xmm1, xmm1 
	call glTexCoord2f wrt ..plt

	; 0,0
	pxor xmm0, xmm0
	pxor xmm1, xmm1		
	call glVertex3f wrt ..plt		

	movd xmm0, dword [dd_one]
	movd xmm1, dword [dd_one]
	call glTexCoord2f wrt ..plt

	;256, 256		
	movd xmm0, dword [dd_rectsize]
	movd xmm1, dword [dd_rectsize]
	call glVertex3f wrt ..plt		

	movd xmm0, dword [dd_one]
	pxor xmm1, xmm1		
	call glTexCoord2f wrt ..plt

	;256, 0
	movd xmm0, dword [dd_rectsize]
	pxor xmm1, xmm1
	call glVertex3f wrt ..plt		
		
	call glEnd wrt ..plt
	call glFlush wrt ..plt

	add rsp, 8
	pop rbx
	pop rbp
	ret

;gradient blue to black, bottom to top
createtex:  xor rsi, rsi

loopbyte:   xor rdx, rdx
            mov rax, rsi
            mov rbx, 192
            div rbx
            shl rax, 2
            inc rsi
            inc rsi
            mov rbx, texture
            mov byte [rbx + rsi], al
            inc rsi
            cmp rsi, 12287
            jle loopbyte
            ret

main:	mov rbp, rsp; for correct debugging

	lea rsp, [rsp-8]
	mov [rsp], edi
	lea rdi, [rsp]

	call glutInit wrt ..plt

	xor rdi, rdi
	call glutInitDisplayMode wrt ..plt

	; The floating point constants we have (dq_screenwidth, dq_screenheight) does not fit here
	mov rdi, 512
	mov rsi, 512
	call glutInitWindowSize wrt ..plt

	mov rdi, 100
	mov rsi, 100
	call glutInitWindowPosition wrt ..plt

	mov rdi, wintitle
	call glutCreateWindow wrt ..plt
	mov [winhandle], rax

	mov rdi, draw
	call glutDisplayFunc wrt ..plt

	mov rdi, [GL_TEXTURE_2D]
	call glEnable wrt ..plt
        
	mov rdi, [GL_SMOOTH]
	call glShadeModel wrt ..plt

	movd xmm0, dword [dd_zero]
	movd xmm1, dword [dd_zero]
	movd xmm2, dword [dd_zero]
	movd xmm3, dword [dd_pointfour]
	call glClearColor wrt ..plt

	movd xmm0, dword [dd_one]
	call glClearDepth wrt ..plt

	mov rdi, [GL_DEPTH_TEST]
	call glEnable wrt ..plt

	mov rdi, [GL_EQUAL]
	call glDepthFunc wrt ..plt

	mov rdi, [GL_PERSPECTIVE_CORRECTION_HINT]
	mov rsi, [GL_NICEST]
	call glHint wrt ..plt
	
	call createtex

	mov rdi, 1
	lea rsi, [dq_texturenum]      
	call glGenTextures wrt ..plt

	mov rdi, [GL_TEXTURE_2D]
	mov rsi, [dq_texturenum]
	call glBindTexture wrt ..plt

	;sequence: rdi, rsi, rdx, rcx, r8, r9, stack reverse
	mov rdi, [GL_TEXTURE_2D]
	mov rsi, [dq_zero]
	mov rdx, 3
	mov rcx, 64
	mov r8, 64
	mov r9, [dq_zero]
	sub rsp, 32
	mov rax, [GL_RGB]       
	mov qword [rsp], rax
	mov rax, [GL_UNSIGNED_BYTE]
	mov qword [rsp + 8], rax
	lea rax, [texture]
	mov qword [rsp + 16], rax
	call glTexImage2D wrt ..plt
	add rsp, 32     

	mov rdi, [GL_TEXTURE_2D]
	mov rsi, [GL_TEXTURE_MIN_FILTER]
	mov rdx, [GL_LINEAR]
	call glTexParameteri wrt ..plt

	mov rdi, [GL_TEXTURE_2D]
	mov rsi, [GL_TEXTURE_MAG_FILTER]
	mov rdx, [GL_LINEAR]
	call glTexParameteri wrt ..plt

	call glutMainLoop wrt ..plt

	xor rax, rax					; return 0 to OS
	lea rsp,[rsp+8]
